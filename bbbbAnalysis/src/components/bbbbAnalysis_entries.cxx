#include "../BaselineVarsBoostedAlg.h"
#include "../BaselineVarsResolvedAlg.h"
#include "../bbbbSelectorAlg.h"
#include "../JetPairingAlg.h"
#include "../JetBoostHistogramsAlg.h"
#include "../MassPlaneBoostHistogramsAlg.h"
#include "../SmallRJetTriggerSFAlg.h"
#include "../TriggerDecoratorAlg.h"

using namespace HH4B;

DECLARE_COMPONENT(BaselineVarsBoostedAlg)
DECLARE_COMPONENT(BaselineVarsResolvedAlg)
DECLARE_COMPONENT(bbbbSelectorAlg)
DECLARE_COMPONENT(JetPairingAlg)
DECLARE_COMPONENT(JetBoostHistogramsAlg)
DECLARE_COMPONENT(MassPlaneBoostHistogramsAlg)
DECLARE_COMPONENT(SmallRJetTriggerSFAlg)
DECLARE_COMPONENT(TriggerDecoratorAlg)
